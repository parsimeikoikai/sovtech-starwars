import { RESTDataSource } from "apollo-datasource-rest";

class StarWarsAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = "https://swapi.dev/api";
  }
  // get list of all people
  async fetchPeople(pageUrl: string) {
    let page = 1;
    if (pageUrl) {
      // Parse the url
      const url = new URL(pageUrl);
      // get  the page number
      page = parseInt(url.searchParams.get("page")!);
    }

    const data = await this.get(`/people/?page=${page}`);
    return data;
  }
  async getIndividualByName(name: String) {
    const data = await this.get(`/people/?search=${name}`);
    if (!data.results.length) {
      return {};
    }
    const person = data.results[0];
    return {
      name: person.name,
      height: person.height,
      mass: person.mass,
      gender: person.gender,
      homeworld: person.homeworld,
    };
  }
}
export default StarWarsAPI;
