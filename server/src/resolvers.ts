const resolvers = {
  Query: {
    people: async (_: any, { pageUrl }: any, { dataSources }: any) => {
      return dataSources.starWarsAPI.fetchPeople(pageUrl);
    },
    person: async (_: any, { name }: any, { dataSources }: any) => {
      return dataSources.starWarsAPI.getIndividualByName(name);
    },
  },
};
export default resolvers;
